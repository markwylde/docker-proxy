# Example Usage
## In docker-compose file
```yml
services:
  gateway:
    build: markwylde/docker-proxy
    ports:
      - 80:80
    command: >
      "
        routes:
          - path: /
            target: http://api/
          - path: /assets/
            target: http://static/
      "

  api:
    build: ../api-server

  static:
    build: ../static-server
```